<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    public function index()
    {
        return Tag::all();
    }

    public function show($id)
    {
        return Tag::findOrFail($id);
    }

    public function save(Request $request)
    {
        if($request->id > 0){
            $tag = Tag::findOrFail($request->id);
            $tag->update($request->all());
        }else{
            $tag = Tag::create($request->all());
        }
        return $tag;
    }

    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();
        return '';
    }
}
